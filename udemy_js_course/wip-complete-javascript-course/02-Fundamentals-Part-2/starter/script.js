"use strict";

/*
///////////////////////////////////////////////////////
// VIDEO 32: ACTIVATING STRICT MODE
let hasDriversLicense = false;
const passTest = true;

if (passTest) hasDriversLicense = true;
if (hasDriversLicense) console.log(`I can drive.`);

// There are reserved JS words that currently exist or might exist in the future that will generate and error to avoid problems in the future.  Only works in STRICT MODE.
// const interface = `Audio`;
// const private = 534;
// if = 23;



///////////////////////////////////////////////////////
// VIDEO 33: FUNCTIONS
function logger() {
    console.log(`My name is Jonas.`)
}

// calling / running / invoking the function
logger();
logger();
logger();

function fruitProcessor(apples, oranges) {
    // console.log(apples, oranges);
    const juice = `Juice with ${apples} apples and ${oranges} oranges.`;
    return juice;
}

const appleJuice = fruitProcessor(5, 0);
console.log(appleJuice);
// console.log(fruitProcessor(5, 0));

const appleOrangeJuice = fruitProcessor(2, 4);
console.log(appleOrangeJuice);

const num = Number(`23`)



///////////////////////////////////////////////////////
// VIDEO 34: FUNCTION DECLARATIONS VS EXPRESSIONS

//// FUNCTION DELCARATION
function calcAge1(birthYear) {
    const age = 2037 - birthYear;
    return age;
}

const age1 = calcAge1(1991);
// console.log(age1);


//// FUNCTION EXPRESSION
// THIS FUNCTION IS IN FACT AN EXPRESSION, WHICH PRODUCES VALUES. WE ASSIGNED THIS VALUE TO THE calcAge2 VARIABLE. THIS VARIABLE WILL HOLD THE FUNCTION VALUE.

// IMPLICATION IS WE CAN CALL FUNCTIONS BEFORE THEY ARE DEFINED. HOWEVER, WE CANNOT DO THE SAME FOR EXPRESSIONS / ANONYMOUS FUNCTIONS. THIS IS DRIVEN BY A PROCESS CALL "HOISTING"

const calcAge2 = function (birthYear) { // ANONYMOUS FUNCTION (I.E. A FUNCTION WITHOUT A NAME)
    return 2037 - birthYear;
}

const age2 = calcAge2(1991);

console.log(age1, age2);

// WHICH FUNCTION SHOULD I USE? PERSONAL PREFERENCE.  FUNCTION EXPRESSIONS HOWEVER FORCE YOU TO KEEP CLEAN STRUCTURE AND DEFINE ALL YOUR FUNCTIONS AT THE TOP OF YOUR CODE AND ONLY THEN CAN YOU CALL THEM. ALSO, FORCES YOUR TO STORE EVERYTHING IN VARIABLES (BOTH VALUES AND FUNCTIONS)




///////////////////////////////////////////////////////
// VIDEO 35: ARROW FUNCTIONS

// Function expression
const calcAge2 = function (birthYear) {
    return 2037 - birthYear;
}

// Arrow function (a special form of function expression)
const calcAge3 = birthYear => 2037 - birthYear;
const age3 = calcAge3(1991);
console.log(age3);

const yearsUntilRetirement = (birthYear, firstName) => {
    const age = 2037 - birthYear;
    const retirement = 65 - age;
    // return retirement;
    return `${firstName} retires in ${retirement} years`
}

console.log(yearsUntilRetirement(1991, "john"));
console.log(yearsUntilRetirement(1980, "bob"));





///////////////////////////////////////////////////////
// VIDEO 36: FUNCTIONS CALLING OTHER FUNCTIONS

function cutFruitPieces(fruit) {
    return fruit * 4;
}

function fruitProcessor(apples, oranges) {
    const applePieces = cutFruitPieces(apples);
    const orangePieces = cutFruitPieces(oranges);

    const juice = `Juice with ${applePieces} pieces of apple and ${orangePieces} pieces of orange.`;
    return juice;
}

console.log(fruitProcessor(2, 3));



///////////////////////////////////////////////////////
// VIDEO 37: REVIEWING FUNCTIONS

const calcAge = function (birthYear) {
    return 2037 - birthYear;
}


const yearsUntilRetirement = function (birthYear, firstName) {
    const age = calcAge(birthYear)
    const retirement = 65 - age;

    if (retirement > 0) {
        return retirement;
    } else {
        return -1;
    }

    return retirement;

    // return `${firstName} retires in ${retirement} years`
}

console.log(yearsUntilRetirement(1991, "john"));
console.log(yearsUntilRetirement(1970, "bob"));

///////////////////////////////////////////////////////
// VIDEO 38: CODING CHALLENGE 1



///////////////////////////////////////////////////////
// VIDEO 39: INTRODUCTION TO ARRAYS

const friend1 = `Michael`;
const friend2 = `Steven`;
const friend3 = `Peter`;

const friends = ['Michael', 'Steven', 'Peter'];
console.log(friends)

const y = new Array(1991, 1984, 2008, 2020);
console.log(y);

console.log(friends[0]);
console.log(friends[2]);

console.log(friends.length);
console.log(friends[friends.length - 1]); // Last value in array

friends[2] = 'Jay'; // Even though variable is "Const", b/c arrays are not primative variables, you can change values within an array, but cannot change the whole array.
console.log(friends);

// friends = ['Bob', 'Alice'] // Cannot change the whole array

const firstName = 'Jonas'
const jonas = [firstName, 'Schmedtmann', 2037 - 1991, 'teacher', friends];
console.log(jonas);
console.log(jonas.length);

// Exercise
const calcAge = function (birthYear) {
    return 2037 - birthYear;
}

const years = [1990, 1967, 2002, 2010, 2018];

console.log(calcAge(years)); // JS cannot calculate function with a array input when it's expecting a single value

const age1 = calcAge(years[0]);
const age2 = calcAge(years[1]);
const age3 = calcAge(years[years.length - 1]);
console.log(age1, age2, age3);

const ages = [calcAge(years[0]), calcAge(years[1]), calcAge(years[years.length - 1])];
console.log(ages);



///////////////////////////////////////////////////////
// VIDEO 40: BASIC ARRAY OPERATIONS (METHODS)

// add elements
//// .push method adds elements to the end of an array
const friends = ['Michael', 'Steven', 'Peter'];
const newLength = friends.push('Jay');
console.log(friends);
console.log(newLength); // .push funtions returns a value --> the length of the array

//// .unshift adds elements to the beginning of an array
friends.unshift('John');
console.log(friends);

// remove elements
//// .pop remove the last element from an array --> opposite of the push method
friends.pop();
console.log(friends);

const popped = friends.pop();
console.log(popped);

//// .shift removeds the first element from an array --> opposite of the unshift method
friends.shift();
console.log(friends);

// .indexOf return index of elements
console.log(friends.indexOf('Steven'));
console.log(friends.indexOf('Bob')); // returns -1, doesn't exist in array

// .includes returns the boolean if element is in / not in the array.  This method uses strict equality and doesn't use type coercion.
console.log(friends.includes('Steven'));
console.log(friends.includes('Bob'));

if (friends.includes('Steven')) {
    console.log('You have a friend called Steven')
}

*/

///////////////////////////////////////////////////////
// VIDEO 41: CODING CHALLENGE 2

///////////////////////////////////////////////////////
// VIDEO 42: INTRODUCTION TO OBJECTS

///////////////////////////////////////////////////////
// VIDEO 43: DOT VS BRACKET NOTATION

// const jonas = {
//   firstName: "Jonas",
//   lastName: "Schmedtmann",
//   age: 2037 - 1991,
//   job: "teacher",
//   friends: ["Michael", "Peter", "Steven"],
// };

// console.log(jonas);

//// NOTE HOW TO RETRIEVE ELEMENTS FROM AN OBJECT
// NOTE The results of bracket and dot notation are exactly the same, but the bracket notation allows you to use expressions within the brackets.
// console.log(jonas.lastName);
// console.log(jonas["lastName"]);

// const nameKey = "Name";
// console.log(jonas["first" + nameKey]);
// console.log(jonas["last" + nameKey]);

// NOTE Let's say you don't yet which property you want to use and instead you get the information from a user interface, we can use the prompt function

// const interestedIn = prompt("What do you want to know about Jonas? Choose between firstName, lastName, age, job, and friends");
// console.log(jonas[interestedIn]);

// if (jonas[interestedIn]) {
//   console.log(jonas[interestedIn]);
// } else {
//   console.log("Wrong request! Choose between firstName, lastName, age, job, and friends");
// }

//// NOTE USE DOT / BRACKET NOTATION TO ADD NEW PROPERTIES TO AN OBJECT
// jonas.location = "Protugal";
// jonas["twitter"] = "@jonas";
// console.log(jonas);

// Challenge
// "Jonas has 3 friends, and his best friend is called Michael"
// NOTE dot notation and brackets are operators and have precedence rules

// const sentence = `${jonas.firstName} has ${jonas.friends.length} friends, and his best friend is called ${jonas.friends[0]}`;
// console.log(sentence);

///////////////////////////////////////////////////////
// VIDEO 44: OBJECT METHODS

// NOTE Objects just like arrays can hold different types of data.  They can hold arrays and objects within objects. Functions are just another type of value, that means that we can create a key-value pair in which the value is a function.  Therefore we can add functions to objects.

// const jonas = {
//   firstName: "Jonas",
//   lastName: "Schmedtmann",
//   birthYear: 1991,
//   age: 2037 - 1991,
//   job: "teacher",
//   friends: ["Michael", "Peter", "Steven"],
//   hasDriversLicense: true,

//   //   calcAge: function (birthYear) {
//   //     return 2037 - birthYear;
//   //   },

//   // NOTE Example of an object (jonas) calling a method / function (calcAge)
//   //   calcAge: function () {
//   //     // console.log(this);
//   //     return 2037 - this.birthYear;
//   //   },
//   // };

//   // NOTE Can use "this" method to calculate function once and then store it as a property in an object to be referenced later when needed
//   calcAge: function () {
//     this.age = 2037 - this.birthYear;
//     return this.age;
//   },

//   // Challenge
//   getSummary: function () {
//     return `${this.firstName} is a ${this.calcAge()}-year old ${jonas.job} and he has ${this.hasDriversLicense ? "a" : "no"} driver's license.`;
//   },
// };

// console.log(jonas.calcAge());

// console.log(jonas.age);
// console.log(jonas.age);
// console.log(jonas.age);

// // Challenge
// // "Jonas is a 46-year old teacher, and he has a driver's license."
// console.log(jonas.getSummary());

// NOTE we can call methods / functions on JS objects AND arrays.  Arrays are a special type of object

///////////////////////////////////////////////////////
// VIDEO 45: CODING CHALLENGE 3

///////////////////////////////////////////////////////
// VIDEO 46: ITERATION: THE FOR LOOP

//NOTE Loop statement has 3 parts [1] Initial value of the counter (rep = 1) [2] A logical condition that is evaluated before each iteration of the loop [3] Increasing / updating the counter after each iteration.

// for loop keeps running while condition is TRUE
// for (let rep = 1; rep <= 10; rep++) {
//   console.log(`Lifting weights repetition ${rep}`);
// }

///////////////////////////////////////////////////////
// VIDEO 47: LOOPING ARRAYS, BREAKING AND CONTINUING

// const jonas = ["Jonas", "Schmedtmann", 2037 - 1991, "teacher", ["Michael", "Peter", "Steven"]];

// const types = [];

// for (let i = 0; i < jonas.length; i++) {
//   console.log(jonas[i], typeof jonas[i]);

//   //NOTE Filling types of array (1 of 3)
//   //   types[i] = typeof jonas[i];

//   //NOTE Filling types of array (2 of 3)
//   types.push(typeof jonas[i]);
// }

// console.log(types);

// //NOTE Filling types of array (3 of 3)
// const years = [1992, 2007, 1969, 2020];
// const ages = [];

// for (let i = 0; i < years.length; i++) {
//   ages.push(2037 - years[i]);
// }

// console.log(ages);

// // NOTE continue and break
// // NOTE CONTINUE: is to exit the current iteration of the loop and continue to the next one. BREAK: is used to completely terminate the whole loop

// console.log("--- ONLY STRINGS ---");
// for (let i = 0; i < jonas.length; i++) {
//   if (typeof jonas[i] !== "string") continue;
//   console.log(jonas[i], typeof jonas[i]);
// }

// console.log("--- BREAK WITH NUMBER ---");
// for (let i = 0; i < jonas.length; i++) {
//   if (typeof jonas[i] === "number") break;
//   console.log(jonas[i], typeof jonas[i]);
// }

///////////////////////////////////////////////////////
// VIDEO 48: LOOPING BACKWARDS AND LOOPS IN LOOPS

// const jonas = ["Jonas", "Schmedtmann", 2037 - 1991, "teacher", ["Michael", "Peter", "Steven"]];

// for (let i = jonas.length - 1; i >= 0; i--) {
//   console.log(i, jonas[i]);
// }

// for (let exercise = 1; exercise < 4; exercise++) {
//   console.log(`----- Starting exercise ${exercise}`);

//   for (let rep = 1; rep < 6; rep++) {
//     console.log(`Lifting weight repetition ${rep}`);
//   }
// }

///////////////////////////////////////////////////////
// VIDEO 49: THE WHILE LOOP

// for (let rep = 1; rep <= 10; rep++) {
//   console.log(`Lifting weights repetition ${rep}`);
// }

//NOTE The while loops is more versatile than the for loop. It can be used in a wider variety of situations b/c it doesn't really need a counter.  All the while loop really needs is the condition to be true to keep running.
//NOTE Whenever you're in a situation where you don't how many iterations a loop will have, you can reach for the while loop.  If, on the other hand, you don't know how many times a loop will run, you will need a counter and will want to use a for loop.  For example when you want to loop over an array, you will know how many elements it will have, therefore you will already know how many iterations you will need.
// let rep = 1;
// while (rep <= 10) {
//   console.log(`Lifting weights repetition ${rep}`);
//   rep++;
// }

// let dice = Math.trunc(Math.random() * 6) + 1;

// while (dice !== 6) {
//   console.log(`You rolled a ${dice}`);
//   dice = Math.trunc(Math.random() * 6) + 1;
//   if (dice === 6) {
//     console.log("Loops is about to end...");
//   }
// }

///////////////////////////////////////////////////////
// VIDEO 50: CODING CHALLENGE //

const bills = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52];

const tips = [];
const totals = [];

const calcTip = function (bill) {
  if (bill >= 50 && bill <= 300) {
    return bill * 0.15;
  } else {
    return bill * 0.2;
  }
};

for (let i = 0; i < bills.length; i++) {
  tips.push(calcTip(bills[i]));
  totals.push(tips[i] + bills[i]);
}

console.log(bills, tips, totals);

const arr = [];
const sum = [];

const calcAverage = function (arr) {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    // sum = sum + arr[i];
    sum += arr[i];
  }
  //   console.log(sum);
  return sum / arr.length;
};
console.log(calcAverage([2, 3, 7]));
console.log(calcAverage(totals));
