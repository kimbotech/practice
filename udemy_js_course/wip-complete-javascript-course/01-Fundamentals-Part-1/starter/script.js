/*
// VIDEO 10: VARIABLE NAME CONVENTIONS

let js = 'amazing';
console.log(40 + 8 + 23 - 10)

console.log('Jonas');
console.log(23);

let firstName = "Matilda"

console.log(firstName);
console.log(firstName);
console.log(firstName);

///////////////////////////////////////////////////////
// VIDEO 12: DATA TYPES
true;
let javascriptIsFun = true;
console.log(javascriptIsFun);

console.log(typeof true);
console.log(typeof javascriptIsFun);
console.log(typeof 23);
console.log(typeof "jonas");

javascriptIsFun = "YES!";
console.log(javascriptIsFun);
console.log(typeof javascriptIsFun);

let year;
console.log(year);
console.log(typeof year);
year = 1991;
console.log(year);
console.log(typeof year);

console.log(typeof null);

///////////////////////////////////////////////////////
// VIDEO 13: LET, CONST, VAR

let age = 30;
age = 31;  // This is called MUTATING variable when we reassign the "age" variable from 30 to 31

const birthYear = 1991;
// birthYear = 1990;

// const job;

var job = "programmer";
job = "teacher";

lastName = "kim";
console.log(lastName)


///////////////////////////////////////////////////////
// VIDEO 14: BASIC OPERATORS

// Math Operators
const now = 2037;
const ageJohn = now - 1991;
const ageSarah = now - 2018;
console.log(ageJohn, ageSarah);

console.log(ageJohn * 2, ageJohn / 10, 2 ** 3)

const firstName = 'john';
const lastName = "kim";
console.log(firstName + " " + lastName);

// Assignment Operators
let x = 10 + 5;
x += 10; // x = x + 10
x *= 4; // x = x * 4
x++; // x = x + 1
x--; // x = x - 1
x--; // x = x - 1
console.log(x)

// Comparison Operators
console.log(ageJohn > ageSarah); // >, <, >=, <=
console.log(ageSarah >= 18);

const isFullAge = ageSarah >= 18;

console.log(now - 1991 > now - 2018)


///////////////////////////////////////////////////////
// VIDEO 15: OPERATOR PRECEDENCE

const now = 2037;
const ageJohn = now - 1991;
const ageSarah = now - 2018;

console.log(now - 1991 > now - 2018);

let x, y;
x = y = 25 - 10 - 5;
console.log(x, y);

const averageAge = (ageJohn + ageSarah) / 2;
console.log(ageJohn, ageSarah);
console.log(averageAge);


///////////////////////////////////////////////////////
// VIDEO 16: CHALLENGE 1

///////////////////////////////////////////////////////
// VIDEO 17: STRINGS AND TEMPLATE LITERALS

const firstName = "jonas";
const job = 'teacher';
const birthYear = 1991;
const year = 2037;

const jonas = "I'm " + firstName + ", a " + (year - birthYear) + " year old " + job + "!";
console.log(jonas);

const jonasNew = `I'm ${firstName}, a ${year - birthYear} year old ${job}!`;
console.log(jonasNew);

console.log(`Just a regular string...`);

console.log("Strings with \n\
multiple \n\
lines");

console.log(`String
multiple
lines`) // Will be useful later when we start building HTML from JS. We can use the backticks to create multiple elements and insert them onto a page dynamically.

///////////////////////////////////////////////////////
// VIDEO 18: TAKING DECISION: IF/ELSE STATEMENTS

const age = 15;

if (age >= 18) {
    console.log(`Sara can start driving license.`)
} else {
    const yearsLeft = 18 - age;
    console.log(`Sarah is too young. Wait another ${yearsLeft} years :)`)
}

const birthYear = 1991;
let century;

if (birthYear <= 2000) {
    century = 20;
} else {
    century = 21;
}
console.log(century);



///////////////////////////////////////////////////////
// VIDEO 20: TYPE CONVERSION AND COERCION

// TYPE CONVERSION
const inputYear = "1991";
console.log(Number(inputYear), inputYear)
console.log(Number(inputYear) + 18)

console.log(String(23), 23)

// TYPE COERCION
console.log(`I am ` + 23 + ` years old`)
console.log(`23` - `10` - 3) // Minus operator converts strings to numbers
console.log(`23` + `10` + 3) // Plus operator converts numbers to strings
console.log(`23` * `2`) // Multiply and divide operators convert string to numbers

// PRACTICE
let n = `1` + 1;
n = n - 1;
console.log(n)

let x = 2 + 3 + 4 + `5`
console.log(x)

let y = `10` - `4` - `3` - 2 + `5`
console.log(y)

///////////////////////////////////////////////////////
// VIDEO 21: TRUTHY AND FALSY VALUES

// 5 falsy value: 0, '', undefined, null, NaN
// truthy values are any non-zero number or not empty string

console.log(Boolean(0));
console.log(Boolean(undefined));
console.log(Boolean("jonas"));
console.log(Boolean({}));
console.log(Boolean(``));
console.log(Boolean(""));


// in practice the conversion of truthy/falsy values to boolean is always implicit not implicit

// in other words it is always type coercion that JS does behind the scenes

// JS does type coercion to boolean in 2 scenarios:
//     1. when using logical operators
//     2. in a logical context (i.e. if/else statement)


const money = 0;
if (money) {
    console.log(`Don't spend it all`);
} else {
    console.log(`You should get a job!`);
}

// USE CASE: to check if a variable is defined or not. used to test if something exists or not

// let height = 100;
let height = 0; // zero is a bug b/c it's a valide height, but will be considered a falsy value.  Can be addressed using logical operators.
// let height;

if (height) {
    console.log(`YAY! Height is defined.`);
} else {
    console.log(`Height is UNDEFINED.`);
}



///////////////////////////////////////////////////////
// VIDEO 22: EQUALITY OPERATORS: == VS ===
const age = `18`;
if (age === 18) console.log(`You just became an adult. (Strict)`);
if (age == 18) console.log(`You just became an adult. (Loose)`);

const favorite = Number(prompt("What's your favorite number?"));
console.log(favorite);
console.log(typeof favorite);

if (favorite === 23) {
    console.log(`Cool! 23 is an amazing number!`)
} else if (favorite === 7) {
    console.log(`7 is also a cool number!`)
} else if (favorite === 9) {
    console.log(`9 is also a cool number!`)
} else {
    console.log(`Number is not 23 or 7 or 9`)
}

if (favorite !== 23) console.log(`Why note 23?`);


///////////////////////////////////////////////////////
// VIDEO 23: BOOLEAN LOGIC

///////////////////////////////////////////////////////
// VIDEO 24: LOGICAL OPERATORS
const hasDriversLicense = true; // A
const hasGoodVision = true; // B

console.log(hasDriversLicense && hasGoodVision);
console.log(hasDriversLicense || hasGoodVision);
console.log(!hasDriversLicense);

// if (hasDriversLicense && hasGoodVision) {
//     console.log(`Sarah is able to drive!`)
// } else {
//     console.log(`Someone else should drive`)
// }

const isTired = false; // C
console.log(hasDriversLicense || hasGoodVision || isTired);

if (hasDriversLicense && hasGoodVision && !isTired) {
    console.log(`Sarah is able to drive!`)
} else {
    console.log(`Someone else should drive`)
}

///////////////////////////////////////////////////////
// VIDEO 25: CODING CHALLENGE #3



///////////////////////////////////////////////////////
// VIDEO 26: THE SWITCH STATEMENT
const day = `friday`;

switch (day) {
    case `monday`: // day === `monday`
        console.log(`Plan course structure`);
        console.log(`Go to coding meetup`);
        break;
    case `tuesday`:
        console.log(`Prepare theory videos`);
        break;
    case `wednesday`:
    case `thursday`:
        console.log(`Write code examples`);
        break;
    case `friday`:
        console.log(`record videos`);
        break;
    case `saturday`:
    case `sunday`:
        console.log(`Enjoy the weekend`);
        break;
    default:
        console.log(`Note a valid day!`)
}

if (day === `monday`) {
    console.log(`Plan course structure (if/else)`);
    console.log(`Go to coding meetup (if/else)`);
} else if (day === `tuesday`) {
    console.log(`Prepare theory videos (if/else)`);
} else if (day === `wednesday` || day === `thursday`) {
    console.log(`Write code examples (if/else)`);
} else if (day === `friday`) {
    console.log(`record videos (if/else)`);
} else if (day === `saturday` || day === `sunday`) {
    console.log(`Enjoy the weekend (if/else)`);
} else {
    console.log(`Note a valid day! (if/else)`)
}


///////////////////////////////////////////////////////
// VIDEO 27: STATEMENTS AND EXPRESSIONS
// No code. Theoretical lecture.
// Expressions produce values.  Statements produce actions, not values.
// There are functions / syntactic situations when JS will expect an expression (i.e. template literals) or a statement.

///////////////////////////////////////////////////////
// VIDEO 28: THE CONDITIONAL (TERNARY) OPERATOR
const age = 16;
age >= 18 ? console.log(`I like to drink wine`) : console.log(`I like to drink water`)

const drink = age >= 18 ? `wine (ternary)` : `water (ternary)`;
console.log(drink)

let drink2;
if (age >= 18) {
    drink2 = `wine (if/else)`;
} else {
    drink2 = `water (if/else)`;
}
console.log(drink2);

console.log(`I like to drink ${age >= 18 ? `wine (ternary in template literal)` : `water (ternary in template literal)`}`)

*/


///////////////////////////////////////////////////////
// VIDEO 29: CODING CHALLENGE #4

///////////////////////////////////////////////////////
// VIDEO 30: JS RELEASES

