///////////////////////////////////////////////////////
// VIDEO 33: FUNCTIONS
console.log(``)
console.log(`///////////////VIDEO 33: FUNCTIONS`)

function describeCountry(country, population, capitalCity) {
  aboutCountry = `${country} has ${population} million people and its capital city is ${capitalCity}`;
  return aboutCountry;
}

const descSouthKorea = describeCountry(`South Korea`, 20, `Seoul`);
const descUnitedStates = describeCountry(`United States`, 330, `Washington DC`);
const descEngland = describeCountry(`England`, 40, `London`);

console.log(`${descSouthKorea}\n${descUnitedStates}\n${descEngland}`);


///////////////////////////////////////////////////////
// VIDEO 34: FUNCTION DECLARATIONS VS EXPRESSIONS
console.log(``)
console.log(`///////////////VIDEO 34: FUNCTION DECLARATIONS VS EXPRESSIONS`)

// function percentageOfWorld1(population) {
//   percentageCalc = (population / 7900) * 100
//   return `${percentageCalc}%`
// }

// function percentageOfWorld1(population) {
//   return (population / 7900) * 100
// }

// console.log(percentageOfWorld1(790));

// const percPortugal1 = percentageOfWorld1(10);
// const percChina1 = percentageOfWorld1(1441);
// const percUSA1 = percentageOfWorld1(332);
// console.log(percPortugal1, percChina1, percUSA1)


///////////////////////////////////////////////////////
// VIDEO 35: ARROW FUNCTIONS
console.log(``)
console.log(`///////////////VIDEO 35: ARROW FUNCTIONS`)

percentageOfWorld1 = population => (population / 7900) * 100;

const percPortugal1 = percentageOfWorld1(10);
const percChina1 = percentageOfWorld1(1441);
const percUSA1 = percentageOfWorld1(332);
console.log(percPortugal1, percChina1, percUSA1)




///////////////////////////////////////////////////////
// VIDEO 36: FUNCTIONS CALLING OTHER FUNCTIONS
console.log(``)
console.log(`///////////////VIDEO 36: FUNCTIONS CALLING OTHER FUNCTIONS`)

function describePopulation(country, population) {
  const percCountry = percentageOfWorld1(population);
  const description = `${country} has ${population} million people, which is about ${percCountry}% of the world.`;
  console.log(description);
}

describePopulation("China", 1441)
describePopulation("US", 330)


///////////////////////////////////////////////////////
// VIDEO 40: BASIC ARRAY OPERATIONS (METHODS)
console.log(``)
console.log(`///////////////VIDEO 40: BASIC ARRAY OPERATIONS (METHODS)`)

const neighbors = ['Canada', 'Mexico'];
neighbors.push('Utopia');
console.log(neighbors);

neighbors.pop();
console.log(neighbors);

if (!neighbors.includes('Germany')) {
  console.log('Probably not a central European country.')
}

const neighborIndex = neighbors.indexOf('Mexico');
console.log(neighborIndex);

neighbors[neighborIndex] = 'Republic of Mexico';
console.log(neighbors);

///////////////////////////////////////////////////////
// VIDEO 42: INTRODUCTION TO OBJECTS
console.log(``)
console.log(`///////////////VIDEO 42: INTRODUCTION TO OBJECTS`)


///////////////////////////////////////////////////////
// VIDEO 43: DOT VS BRACKET NOTATION
console.log(``)
console.log(`///////////////VIDEO 43: DOT VS BRACKET NOTATION`)


///////////////////////////////////////////////////////
// VIDEO 44: OBJECT METHODS
console.log(``)
console.log(`///////////////VIDEO 44: OBJECT METHODS`)


///////////////////////////////////////////////////////
// VIDEO 46: ITERATION: FOR LOOP
console.log(``)
console.log(`///////////////VIDEO 46: ITERATION: FOR LOOP`)


///////////////////////////////////////////////////////
// VIDEO 47: LOOPING ARRAYS, BREAKING AND CONTINUING
console.log(``)
console.log(`///////////////VIDEO 47: LOOPING ARRAYS, BREAKING AND CONTINUING`)


///////////////////////////////////////////////////////
// VIDEO 48: LOOPING BACKWARDS AND LOOPS IN LOOPS
console.log(``)
console.log(`///////////////VIDEO 48: LOOPING BACKWARDS AND LOOPS IN LOOPS`)


///////////////////////////////////////////////////////
// VIDEO 49: WHILE LOOP
console.log(``)
console.log(`///////////////VIDEO 49: WHILE LOOP`)


