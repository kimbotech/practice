///////////////////////////////////////////////
console.log("/////// CODING CHALLENGE 1");

const calcAverage = (score1, score2, score3) => (score1 + score2 + score3) / 3;

const avgDolphins = calcAverage(85, 54, 41);
const avgKoalas = calcAverage(23, 34, 27);

const checkWinner = (avgDolphins, avgKoalas) => {
  if (avgDolphins >= 2 * avgKoalas) {
    return `Dolphins win (${avgDolphins} vs ${avgKoalas})`;
  } else if (avgKoalas >= 2 * avgDolphins) {
    return `Koalas win (${avgKoalas} vs ${avgDolphins})`;
  } else {
    return `No winners this time`;
  }
};

console.log(avgDolphins);
console.log(avgKoalas);
console.log(checkWinner(avgDolphins, avgKoalas));

///////////////////////////////////////////////
console.log("\n/////// CODING CHALLENGE 2");

const calcTip = function (bill) {
  if (bill >= 50 && bill <= 300) {
    return bill * 0.15;
  } else {
    return bill * 0.2;
  }
};

console.log(calcTip(50));

const bills = [125, 550, 44];

const tips = [calcTip(bills[0]), calcTip(bills[1]), calcTip(bills[2])];
console.log(tips);

const total = [bills[0] + calcTip(bills[0]), bills[1] + calcTip(bills[1]), bills[2] + calcTip(bills[2])];
console.log(total);

///////////////////////////////////////////////
console.log("\n/////// CODING CHALLENGE 3");

const markMiller = {
  fullName: "Mark Miller",
  mass: 78,
  height: 1.69,
  BMI: 0,

  calcBMI: function () {
    this.BMI = this.mass / (this.height * this.height);
    return this.BMI;
  },
};

console.log(markMiller.calcBMI());
console.log(markMiller.BMI);

const johnSmith = {
  fullName: "John Smith",
  mass: 92,
  height: 1.95,
};
