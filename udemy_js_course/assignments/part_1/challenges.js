/*
// CHALLENGE 1

let markMass, markHeight, johnMass, johnHeight;

// DATA 1

// markMass = 78;
// markHeight = 1.69;

// johnMass = 92;
// johnHeight = 1.95;


// DATA 2

markMass = 95;
markHeight = 1.88;

johnMass = 85;
johnHeight = 1.76;

// FORMULAS

let markBodyMassIndex = markMass / markHeight ** 2;
let johnBodyMassIndex = johnMass / johnHeight ** 2;

let markHigherBMI = markBodyMassIndex > johnBodyMassIndex;

console.log(markBodyMassIndex, johnBodyMassIndex, markHigherBMI);

/////////////////////////////////////////////////////
// CHALLENGE 2

if (markHigherBMI) {
    console.log(`Mark's BMI (${markBodyMassIndex}) is higher than John's (${johnBodyMassIndex})!`)
} else {
    console.log(`John's BMI (${johnBodyMassIndex})is higher than Mark's (${markBodyMassIndex})!`)
}

*/

/////////////////////////////////////////////////////
// CHALLENGE 3

const minScore = 100;

// const dolphinsAverageScore = (96 + 108 + 89) / 3;
const dolphinsAverageScore = 102;
console.log(dolphinsAverageScore);
const dolphinsAvgAboveMin = dolphinsAverageScore >= minScore;
console.log(dolphinsAvgAboveMin);

// const koalasAverageScore = (88 + 91 + 110) / 3;
const koalasAverageScore = 101;
console.log(koalasAverageScore);
const koalasAvgAboveMin = koalasAverageScore >= minScore;
console.log(koalasAvgAboveMin);

if (!koalasAvgAboveMin && !dolphinsAvgAboveMin) {
    console.log(`No team meets minimum score requirement.`);
} else if (dolphinsAverageScore === koalasAverageScore) {
    console.log(`It's a draw.`);
} else if (dolphinsAverageScore > koalasAverageScore && dolphinsAvgAboveMin) {
    console.log(`Dolphins win!`);
} else if (koalasAverageScore > dolphinsAverageScore && koalasAvgAboveMin) {
    console.log(`Koalas win!`);
}


/////////////////////////////////////////////////////
// CHALLENGE 4
console.log(``)
console.log(`/////////////// PART 1: CHALLENGE 4`)

const bill = 430;
let tip = bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;

console.log(`The bill was ${bill}, the tip was ${tip}, and the total value ${bill + tip}`);

