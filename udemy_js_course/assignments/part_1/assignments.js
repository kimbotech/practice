///////////////////////////////////////////////////////
// VIDEO 10: Values and Variables
const country = "United States";
const continent = "North America";
let population = "330";

/*
console.log(country)
console.log(continent)
console.log(population)
*/

///////////////////////////////////////////////////////
// VIDEO 11: DATA TYPES
const isIsland = false;
let language;

console.log(isIsland);
console.log(population);
console.log(country);
console.log(language);

language = "english";
console.log(language);

///////////////////////////////////////////////////////
// VIDEO 14: BASIC OPERATORS
// population /= 2;
// console.log(population);

// population += 1;
// console.log(population);

let finlandPopulation = 6;
console.log(population > finlandPopulation);

let avgCountryPopulation = 33;
console.log(population > avgCountryPopulation);

// let description = 'The ' + country + ' is in ' + continent + ', and its ' + population + ' million people speak ' + language
// console.log(description);

///////////////////////////////////////////////////////
// VIDEO 17: TEMPLATE LITERALS

let description = `The ${country} is in ${continent} and its ${population} million people speak ${language}`
console.log(description);

// VIDEO 18: TAKING DECISIONS - IF/ELSE
if (population > avgCountryPopulation) {
    console.log(`The ${country}'s population is above average.`)
} else {
    let amountPopulationBelowAverage = avgCountryPopulation - population
    console.log(`The ${country}'s population is ${amountPopulationBelowAverage} million below average.`)

}

//
///////////////////////////////////////////////////////
// VIDEO 20: TYPE CONVERSION AND COERCION

let a = "9" - "5"; // output: 4
let b = "19" - "13" + "17"; // output: 617
let c = "19" - "13" + 17; // output: 23
let d = "123" < 57; // output: false
let e = 5 + 6 + "4" + 9 - 4 - 2; // output: 1143

console.log(a)
console.log(b)
console.log(c)
console.log(d)
console.log(e)

///////////////////////////////////////////////////////
// VIDEO 22: EQUALITY OPERATORS

// console.log(`///////////////VIDEO 22: EQUALITY OPERATORS`)

// const numNeighbors = Number(prompt(`How many neighbor countries does your country have?`))

// if (numNeighbors === 1) {
//     console.log(`Only ${numNeighbors} border!`)
// } else if (numNeighbors > 1) {
//     console.log(`More than 1 border`)
// } else {
//     console.log(`No borders`)
// };


///////////////////////////////////////////////////////
// VIDEO 24: LOGICAL OPERATORS
console.log(`///////////////VIDEO 24: LOGICAL OPERATORS`)

const speaksEnglish = language === "english";
const lessThan50mPopulation = population < 50;

if (speaksEnglish && lessThan50mPopulation && !isIsland) {
    console.log(`You should live in ${country}!`)
} else {
    console.log(`${country} does not meet your criteria`)
}

///////////////////////////////////////////////////////
// VIDEO 26: SWITCH STATMENT
console.log(``)
console.log(`///////////////VIDEO 26: SWITCH STATEMENT`)

switch (language) {
    case `chinese`:
    case `mandarin`:
        console.log(`MOST number of native speakers!`);
        break;
    case `spanish`:
        console.log(`2nd place in number of native speakers!`);
        break;
    case `english`:
        console.log(`3rd place`);
        break;
    case `hindi`:
        console.log(`Number 4`);
        break;
    case `arabic`:
        console.log(`5th most spoken language`);
        break;
    default:
        console.log(`Great language too :D`);
}


///////////////////////////////////////////////////////
// VIDEO 28: CONDITIONAL (TERNARY) OPERATOR
console.log(``)
console.log(`///////////////VIDEO 26: SWITCH STATEMENT`)

population > avgCountryPopulation ? console.log(`The ${country}'s population is above average`) : console.log(`The ${country}'s population is below average`)

console.log(`The ${country}'s population is ${population > avgCountryPopulation ? `above` : `below`} average`)

///////////////////////////////////////////////////////
// VIDEO 26: SWITCH STATMENT